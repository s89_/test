require "csv"

class MultipartGrouper
  def initialize(input_file:, group_files:)
    @input_file = input_file
    @group_files = group_files
  end

  # Implement this method
  def group
    input_data = CSV.read(@input_file)
    group_data = @group_files.map { |group_file| CSV.read(group_file) }
    grouped_studies = Array.new(group_data.length) { Array.new }

    input_data.drop(1).each do |study|
      group_data.each_with_index do |group, index|
        if group.include?([study[1]]) && !in_other_group?(grouped_studies, index, study[1])
          grouped_studies[index] << study[1]
        end
      end
    end

    # Check if there are remainders and if so, try to group them with another study
    remainders = grouped_studies.select { |group| group.length == 1 }
    # Remove empty arrays and groups with only one study
    grouped_studies = grouped_studies.reject { |group| group.empty? || group.length == 1 }

    if remainders.length > 0
      remainders.each do |remainder_study|
        catch :remainder_grouped do
          grouped_studies.each_with_index do |group, group_index|
            group.each do |study|
              if can_be_grouped?(study, remainder_study[0], group_data) == true
                remainder_study << study
                grouped_studies << remainder_study
                group.delete(study)
                throw :remainder_grouped
              end
            end
          end
        end
      end
    end

    return grouped_studies
  end

  private

  def in_other_group?(grouped_studies, current_group_index, study)
    grouped_studies.each_with_index do |group, index|
      next if index == current_group_index
      if group.include?(study)
        return true
      end
    end
    return false
  end

  def can_be_grouped?(study, remainder_study, group_data)
    group_data.each do |group|
      if group.include?([study]) && group.include?([remainder_study])
        return true
      end
    end
  end

  def input_csv
    @input_csv ||= CSV.read(
      @input_file,
      headers: true
    )
  end

  def group_csvs
    @group_csvs ||=
      @group_files.map do |group_file|
        [
          group_file.split("/").last,
          CSV.read(
            group_file,
            headers: true
          )
        ]
      end
  end
end
